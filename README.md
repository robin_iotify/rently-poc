# README #

This repository contains the resources and scripts created for the Rently PoC.

### Template Archive ###

The `template-archive` folder contains all the templates used for the PoC. The versions `-vX` indicates a checkpoint in case we need to restore to an older version. Unfortunately, there is no changelog or commit mechanism integrated. 

### Scripts ###
- The helper scripts are present in the `scripts` folder. 
- Run `npm install` form within the folder to install dependencies
- Run `npm i -g .` within the same folder to install the Rently CLI globally
- Run `rentlyctl --help` to view the CLI Help 

### Who do I talk to? ###

* Repo Maintainer : Robin
* Contributors : Jayraj, Akash
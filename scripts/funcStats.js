const axios = require('axios');

require('dotenv').config()
let token = process.env.GLOB_TOKEN || '';


setInterval(() => {
    axios.get('https://api.iotify.io/v1/glob/gateway_0_stats', {
        headers: {
            'x-auth': token
        }
    }).then(function (response) {
        // console.log(response.data.result);
        let result = JSON.parse(response.data.result);
        console.table(result.functionCounter);
        console.log("Errors : ",result.errors);
        console.log("GW Status : ",result.GWstatus);
        console.log("Last Payload : ",result.lastPayload);
        console.log("Last Delta : ",result.lastDelta);
        // console.table(response.data.result.functionCounter);
    }).catch(function (error) {
        console.log(error);
    })
}, 3000);


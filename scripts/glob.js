#!/usr/bin/env node

const axios = require('axios');


fs = require('fs');
require('dotenv').config()
let token = process.env.GLOB_TOKEN || '';

require('yargs')
    .usage('$0 <cmd> [args]')
    .command('get <key> [file]', 'get Glob Object', (yargs) => {
        yargs.positional('key', {
            type: 'string',
            describe: 'the Key of the object you wish to get',
        }),
            yargs.positional('file', {
                type: 'string',
                describe: 'the file to store the object JSON in'
            })
    }, function (argv) {
        console.log('Output saved to ', argv.file)

        axios.get(`https://api.iotify.io/v1/glob/${argv.key}`, {
            headers: {
                'x-auth': token
            }
        }).then(function (response) {
            // console.log(response.data.result);
            let result = JSON.parse(response.data.result);
            if (!!argv.file) {
                fs.writeFileSync(argv.file, JSON.stringify(result, null, 4));
            }
            else {
                console.log(result);
            }
        }).catch(function (error) {
            console.log(error.response.data);
        })
    })
    .command('set <key> [json] [file]', 'set Glob Object', (yargs) => {
        yargs.positional('key', {
            type: 'string',
            describe: 'the Key of the object you wish to set'
        }), yargs.positional('json', {
            type: 'string',
            describe: 'JSON Object to set to the Glob Key'
        }),
            yargs.positional('file', {
                type: 'string',
                describe: 'source JSON from file'
            })
    }, function (argv) {
        // console.log('Key', argv.key)
        // console.log('JSON', argv.json)
        // console.log('file', argv.file)

        let postData = {};
        if (!!argv.json) {
            postData = JSON.parse(argv.json)
        }
        else if (!!argv.file) {
            postData = JSON.parse(fs.readFileSync(argv.file))
        }
        else
            throw new Error('No JSON or File specified');

        let axiosOptions = {
            headers: {
                'x-auth': 'bff08f63b59b3e7bfe213cada0dcc92d64dfe204778cc9cc',
                "Content-type": "application/json"
            }
        }
        let url = "https://api.iotify.io/v1/glob/" + argv.key;
        axios.post(url, postData, axiosOptions)
            .then(function (response) {
                console.log("success");
            }).catch(function (error) {
                console.log(error);
            })
    })
    .command('delete <key>', 'delete Glob Object', (yargs) => {
        yargs.positional('key', {
            type: 'string',
            describe: 'the Key of the object you wish to delete',
        })
    }, function (argv) {
        console.log('Deleting Glob Object with Key : ', argv.key)

        axios.delete(`https://api.iotify.io/v1/glob/${argv.key}`, {
            headers: {
                'x-auth': token
            }
        }).then(function (response) {
            console.log("success");
        }).catch(function (error) {
            console.log(error);
        })
    })
    .command('token <apitoken>', 'Set IoTIFY API Token', (yargs) => {
        yargs.positional('apitoken', {
            type: 'string',
            describe: 'IoTIFY API Token',
        })
    }, function (argv) {
        // console.log('apitoken', argv.apitoken)
        
        fs.writeFileSync('.env', "GLOB_TOKEN="+argv.apitoken);
        console.log('Token set to', argv.apitoken);
    })

    .help()
    .argv
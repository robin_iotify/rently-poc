#!/usr/bin/env node

const yargs = require("yargs");
const axios = require('axios');

require('dotenv').config()
let token = process.env.GLOB_TOKEN || '';

let globKey = "";
let postData = {};

const options = yargs
    .usage("Usage: -g <gateway client id> -s <online/offline>")
    .option("g", { alias: "clientId", describe: "gateway client id", type: "string", demandOption: true })
    .option("s", { alias: "status", describe: "post_boot/offline", type: "string" })
    .option("m", { alias: "manual", describe: "manual trigger", type: "string" })
    .argv;

if (options.status) {
    const msg = `Setting Gateway_${options.clientId} to ${options.status}`;
    console.log(msg);
    globKey = "gateway_" + options.clientId + "_status";
    postData = { value: options.status }
}
else if (options.manual) {
    const msg = `Triggering ${options.manual} on Gateway_${options.clientId}`;
    console.log(msg);
    globKey = "gateway_" + options.clientId + "_triggerManual";
    postData = { value: options.manual }
}
else{
    throw new Error("Set Status or trigger manual");
}

let axiosOptions = {
    headers: {
        'x-auth': token,
        "Content-type": "application/json"
    }
}


let url = "https://api.iotify.io/v1/glob/" + globKey;



axios.post(url, postData, axiosOptions)
    .then(function (response) {
        console.log("success");
    }).catch(function (error) {
        console.log(error);
    })